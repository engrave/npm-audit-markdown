module.exports = {
    "semi": true,
    "trailingComma": "none",
    "arrowParens": "always",
    "singleQuote": true,
    "printWidth": 170,
    "tabWidth": 4,
    "useTabs": false,
    "bracketSpacing": false,
    "endOfLine": "lf",
    "overrides": [
        {
            "files": ["*.json"],
            "options": {
                "tabWidth": 2,
                "useTabs": false
            }
        }
    ]
}

