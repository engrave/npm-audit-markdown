import {program} from 'commander';
import * as updateNotifier from 'update-notifier';
import * as fs from 'fs-extra';
import {generateAndSaveReportFile} from './lib/report';
import {ParserError} from './lib/parsers/ParserError';

import * as pkg from '../package.json';

updateNotifier({pkg}).notify();

let stdin = '';

program
    .version(pkg.version)
    .option('-o, --output [output]', 'output file')
    .option('-i, --input [input]', 'input file')
    .action(async (cmd) => {
        try {
            let data;
            if (cmd.input) {
                data = await fs.readJson(cmd.input);
            } else if (stdin) {
                data = JSON.parse(stdin);
            } else {
                console.log('No input');
                return process.exit(1);
            }
            process.exit(await generateAndSaveReportFile(data, cmd.output));
        } catch (error) {
            if (error instanceof ParserError) {
                console.error(error.message);
            } else {
                console.error('Failed to parse NPM Audit JSON!');
            }
            return process.exit(1);
        }
    });

if (process.stdin.isTTY) {
    program.parse(process.argv);
} else {
    process.stdin.on('readable', function () {
        const chunk = this.read();
        if (chunk !== null) {
            stdin += chunk;
        }
    });
    process.stdin.on('end', function () {
        program.parse(process.argv);
    });
}
