import {DependenciesMeta, NormalizedAuditReport, SuggestedAction, VulnerabilitiesMeta} from '../NormalizedAuditReport';

const v6 = (data): NormalizedAuditReport => ({
    vulnerabilities: Object.values(data.advisories).map(normalizeVulnerabilities),
    actions: data.actions.map(proposeAction),
    metadata: {
        dependencies: normalizeDependencies(data.metadata),
        vulnerabilities: normalizeVulnerabilitiesMeta(data.metadata)
    }
});

const normalizeVulnerabilities = (vulnerability) => {
    return {...vulnerability, recommendation: vulnerability.recommendation ? vulnerability.recommendation.replace('\n', ' ') : ''};
};

const normalizeDependencies = (metadata): DependenciesMeta => ({
    prod: metadata.prodDependencies ?? 0,
    dev: metadata.devDependencies ?? 0,
    optional: metadata.optionalDependencies ?? 0,
    peer: metadata.peerDependencies ?? 0,
    peerOptional: metadata.peerOptionalDependencies ?? 0,
    total: metadata.totalDependencies ?? 0
});

const normalizeVulnerabilitiesMeta = (metadata): VulnerabilitiesMeta => {
    const vulnerabilities = metadata.vulnerabilities || [];

    let total = 0;
    for (const vul in vulnerabilities) {
        const count = vulnerabilities[vul];
        total += count;
    }
    return {
        ...metadata.vulnerabilities,
        total
    };
};

const proposeAction = (action): SuggestedAction => {
    const isDev = action.resolves.reduce((result, v) => v.dev && result, true);
    switch (action.action) {
        case 'install':
            return {...action, command: `npm install ${isDev ? '--save-dev' : ''} ${action.module}@${action.target}`};
        case 'review':
            return {...action, command: 'Manual review'};
        case 'update':
            return {...action, command: `npm update ${action.module} --depth ${findActionResolvesDepth(action)}`};
        default:
            return {...action, command: 'Manual review'};
    }
};

const findActionResolvesDepth = (action) =>
    action.resolves.reduce((result, v) => {
        const depth = v.path.split('>').length - 1;
        return depth > result ? depth : result;
    }, 0);

export default v6;
