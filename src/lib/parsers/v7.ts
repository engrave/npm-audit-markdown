import {NormalizedAuditReport, Vulnerability} from '../NormalizedAuditReport';

const v7 = (data: Record<string, any>): NormalizedAuditReport => {
    return {
        vulnerabilities: normalizeVulnerabilities(data),
        actions: [],
        metadata: {
            vulnerabilities: data.metadata?.vulnerabilities,
            dependencies: data.metadata?.dependencies
        }
    };
};

function normalizeVulnerabilities(data: Record<string, any>) {
    const vulnerabilities = [];

    Object.entries<any>(data.vulnerabilities).forEach(([packageName, properties]) => {
        const recommendation = determineFix(properties.fixAvailable);
        properties.via.forEach((via) => {
            if (typeof via === 'object') {
                vulnerabilities.push({...via, recommendation});
            }
        });
    });

    const unique = [];

    for (const v of vulnerabilities) {
        if (!unique.find((item) => item.source === v.source)) {
            unique.push(v);
        }
    }

    return unique.map((reason): Vulnerability => {
        return {
            title: reason.title,
            url: reason.url,
            module_name: reason.name,
            severity: reason.severity,
            cwe: [],
            cves: [],
            recommendation: reason.recommendation
        };
    });
}

const determineFix = (fixAvailable: boolean | Record<string, any>): string => {
    if (typeof fixAvailable === 'object') {
        return `Update ${fixAvailable.name} to ${fixAvailable.version}`;
    }
    if (fixAvailable === true) {
        return 'Run `npm audit fix`';
    }
    return `Manual review`;
};

export default v7;
