export class ParserError extends Error {
    constructor(public readonly message: string, public readonly cause?: Error) {
        super(message);
        this.name = ParserError.name;
        this.stack = new Error().stack;
    }
}
