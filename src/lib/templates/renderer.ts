import * as doT from 'dot';
import {template} from './template';
import {NormalizedAuditReport} from '../NormalizedAuditReport';

const renderer = doT.template(template, {
    evaluate: /{{([\s\S]+?)}}/g,
    interpolate: /{{=([\s\S]+?)}}/g,
    encode: /\{\{!([\s\S]+?)\}\}/g,
    use: /\{\{#([\s\S]+?)\}\}/g,
    define: /{{##\s*([\w.$]+)\s*(:|=)([\s\S]+?)#}}/g,
    conditional: /\{\{\?(\?)?\s*([\s\S]*?)\s*\}\}/g,
    iterate: /\{\{~\s*(?:\}\}|([\s\S]+?)\s*:\s*([\w$]+)\s*(?::\s*([\w$]+))?\s*\}\})/g,
    varname: 'it',
    useParams: undefined,
    defineParams: undefined,
    strip: false,
    append: true,
    selfcontained: false
});

const renderTemplate = (data: NormalizedAuditReport) => renderer(data);

export {renderTemplate};
