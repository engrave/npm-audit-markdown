import * as chalk from 'chalk';
import v6 from './parsers/v6';
import v7 from './parsers/v7';
import {ParserError} from './parsers/ParserError';
import {renderTemplate} from './templates/renderer';
import * as fs from 'fs-extra';

const generateAndSaveReportFile = async (input: Record<string, unknown>, outputFilePath = 'npm-audit.md'): Promise<number> => {
    if (!input) {
        throw new ParserError('No JSON provided');
    }

    if (!input.metadata) {
        if (input.updated) {
            throw new ParserError(`Sorry! You can't use ${chalk.underline('npm audit fix')} with npm-audit-markdown.`);
        }
        throw new ParserError(`The provided data doesn't seem to be correct. Did you run with ${chalk.underline('npm audit --json')}?`);
    }

    const parser = input.auditReportVersion ? v7 : v6;

    const data = parser(input);
    const content = renderTemplate(data);
    await saveReportFile(content, outputFilePath);

    console.log(`Vulnerability snapshot saved at ${outputFilePath}`);
    return data.metadata.vulnerabilities.total ? 1 : 0;
};

const saveReportFile = async (content: string, filepath: string) => {
    await fs.ensureFile(filepath);
    await fs.writeFile(filepath, content);
};

export {generateAndSaveReportFile};
