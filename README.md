# npm-audit-markdown

A simple tool to create an NPM audit report in markdown language. Especially useful to post reports to communicators supporting markdown.

## Getting Started

```shell script
npm install -g npm-audit-markdown
```

## Usage

To generate a report, run the following:

```shell script
npm audit --json | npm-audit-markdown
```

By default the report will be saved to npm-audit.md

If you want to specify the output file, add the --output option:

```shell script
npm audit --json | npm-audit-markdown --output report.md
```

## Acknowledgements

Forked from https://github.com/eventOneHQ/npm-audit-html

## Licence
MIT
